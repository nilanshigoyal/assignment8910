import java.util.ArrayList;

import com.google.gson.GsonBuilder;

public class main {

	public static void main(String[] args) {
		int difficulty = 5;
		ArrayList<Block> blockchain = new ArrayList<Block>();
		blockchain.add(new Block("First block", "0"));
		System.out.println("trying to mine block 1");
		blockchain.get(0).mineBlock(difficulty);
		blockchain.add(new Block("Second Block", (blockchain.get(blockchain.size() - 1).hash)));
		System.out.println("trying to mine block 2");
		blockchain.get(0).mineBlock(difficulty);
		blockchain.add(new Block("Third Block", (blockchain.get(blockchain.size() - 1).hash)));
		System.out.println("trying to mine block 3");
		blockchain.get(0).mineBlock(difficulty);
		blockchain.add(new Block("Forth Block", (blockchain.get(blockchain.size() - 1).hash)));
		System.out.println("trying to mine block 4");
		blockchain.get(0).mineBlock(difficulty);
		System.out.println("\nBlock chain is valid:" + isChainValid());
		String blockChainJson = new GsonBuilder().setPrettyPrinting().create().toJson(blockchain);
		System.out.println(blockChainJson);
	}

	public static Boolean isChainValid() {
		Block currentBlock;
		Block previousBlock;
		int difficulty = 5;
		String hashTarget = new String(new char[difficulty]).replace('\0', '0');
		ArrayList<Block> blockchain = new ArrayList<Block>();
		for (int i = 1; i < blockchain.size(); i++) {
			currentBlock = blockchain.get(i);
			previousBlock = blockchain.get(i - 1);
			if (!currentBlock.hash.contentEquals(currentBlock.calculatedHash())) {
				System.out.println("Current Hashes not equal");
				return false;

			}
			if (!previousBlock.hash.contentEquals(currentBlock.previousHash)) {
				System.out.println("Previous Hashes not equal");
				return false;

			}
			if (!currentBlock.hash.substring(0, difficulty).equals(hashTarget)) {
				System.out.println("This block has not been mines");
				return false;
			}

		}
		return true;
	}

}
